# Ultimate PET. GroopieHQs #

## API ##

### Authentication ###

Todos los usuarios tienen un auth_token propio que servirá para poder realizar peticiones a la API de forma logeada.

Si no se envía un auth_token correcto en cada petición, la API devolverá el siguiente mensaje:

```ruby
json: {"errors" => "User doesn't exists. Try to change auth_token params"}
```

### Create Pets ###

Cualquier usuario que autentique su identidad podrá crear nuevas mascotas a través de la API. Automáticamente, esa mascota le será asignada.

Un ejemplo (HTTParty):

```ruby
url = 'localhost:3000/api/v1/pets'
HTTParty.post(url + "?auth_token=Rztg_jPUEcXubVrWepHr&name=Mascota&age=3&sex=girl&type=Dog")
```
Si todo va bien, la API devolverá

```ruby
json: { pet.name => "Created successfully!" }
```
### Destroy Pets ###

Sólo el dueño de la mascota puede destruir una mascota. Para eso existe un método que nos indica si un usuario es dueño o no.

```ruby
def is_owner? pet
  return pet.user == self ? true : false
end
```

Un ejemplo (HTTParty):
```ruby
url = 'localhost:3000/api/v1/pets'
HTTParty.delete(url + "/:id?auth_token=Rztg_jPUEcXubVrWepHr")
```

Si la mascota es destruida con éxito, la API devuelve:

```ruby
json: { "result" => "Pet destroy successfully!" }
```


### Update Pets ###

Sólo el dueño de una mascota puede alterar la información de la misma. Se realizan las mismas comprobaciones que en el método destroy

Un ejemplo (HTTParty):
```ruby
url = 'localhost:3000/api/v1/pets'
HTTParty.delete(url + "/:id?auth_token=Rztg_jPUEcXubVrWepHr&name=Juana")
```

Si la información se actualiza correctamente la API devolverá:
```ruby
json: { @pet.name => "Updated successfully!" }
```

### Show Pets ###

Cualquier usuario puede ver la información de cualquier mascota.

Un ejemplo (HTTParty):

```ruby
url = 'localhost:3000/api/v1/pets'
HTTParty.get(url + "/:id?auth_token=Rztg_jPUEcXubVrWepHr")
```

## Pet Types ##

Cómo existen 3 tipos bien definidos de mascotas, he pensado que una buena solución podría ser utilizar la herencia tal y cómo Rails nos la ofrece. Los modelos querían así:

```ruby
class Pet < ActiveRecord::Base
end

class Dog < Pet
end

class Chinchilla < Pet
end

class Rat < Pet
end
```





***** Durante el desarrollo, ninguna mascota ha sido dañada