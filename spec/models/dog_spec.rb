require 'rails_helper'

RSpec.describe Dog, type: :model do
  it 'has a valid factory' do
    expect(build(:dog)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:dog, name: nil)).to_not be_valid
  end

  it 'is invalid without a type' do
    expect(build(:dog, type: nil)).to_not be_valid
  end

  it 'is valid without a sex' do
    expect(build(:dog, sex: nil)).to be_valid
  end

  it 'is valid without a age' do
    expect(build(:dog, age: nil)).to be_valid
  end
end
