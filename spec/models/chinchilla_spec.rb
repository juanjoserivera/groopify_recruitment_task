require 'rails_helper'

RSpec.describe Chinchilla, type: :model do
  it 'has a valid factory' do
    expect(build(:chinchilla)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:chinchilla, name: nil)).to_not be_valid
  end

  it 'is invalid without a type' do
    expect(build(:chinchilla, type: nil)).to_not be_valid
  end

  it 'is valid without a sex' do
    expect(build(:chinchilla, sex: nil)).to be_valid
  end

  it 'is valid without a age' do
    expect(build(:chinchilla, age: nil)).to be_valid
  end
end
