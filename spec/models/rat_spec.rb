require 'rails_helper'

RSpec.describe Rat, type: :model do
  it 'has a valid factory' do
    expect(build(:rat)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:rat, name: nil)).to_not be_valid
  end

  it 'is invalid without a type' do
    expect(build(:rat, type: nil)).to_not be_valid
  end

  it 'is valid without a sex' do
    expect(build(:rat, sex: nil)).to be_valid
  end

  it 'is valid without a age' do
    expect(build(:rat, age: nil)).to be_valid
  end
end
