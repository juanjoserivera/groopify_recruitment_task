require 'rails_helper'

RSpec.describe UserMetadatum, type: :model do
  it 'has a valid factory' do
    expect(build(:user_metadatum)).to be_valid
  end

  it 'is valid without a first_name' do
    expect(build(:user_metadatum, first_name: nil)).to be_valid
  end

  it 'is valid without a last_name' do
    expect(build(:user_metadatum, last_name: nil)).to be_valid
  end
end
