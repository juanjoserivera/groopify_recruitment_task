require 'rails_helper'

RSpec.describe Pet, type: :model do
  it 'has a valid factory' do
    expect(build(:pet)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:pet, name: nil)).to_not be_valid
  end

  it 'is invalid without a type' do
    expect(build(:pet, type: nil)).to_not be_valid
  end

  it 'is valid without a sex' do
    expect(build(:pet, sex: nil)).to be_valid
  end

  it 'is valid without a age' do
    expect(build(:pet, age: nil)).to be_valid
  end
end
