FactoryGirl.define do
  factory :chinchilla do
    name Faker::Name.name
    age Faker::Number.between(1, 50)
    sex ["boy", "girl"].sample
    victories Faker::Number.between(1, 100)
    user User.all.sample
    type "Chinchilla"
  end
end
