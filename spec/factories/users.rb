FactoryGirl.define do
  factory :user do
    auth_token { Devise.friendly_token } 
    email { Faker::Internet.email }
    password { Faker::Internet.password }
  end

end
