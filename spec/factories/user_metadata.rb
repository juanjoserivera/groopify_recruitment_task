FactoryGirl.define do
  factory :user_metadatum do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    user nil
  end

end
