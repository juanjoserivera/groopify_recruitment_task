FactoryGirl.define do
  factory :fight do
    time Faker::Time.between(DateTime.now, DateTime.now + 3)
    winner_id build(:pet).id
  end
end
