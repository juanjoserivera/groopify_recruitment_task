class UserMailer < ApplicationMailer
  default :from => "no-responder@groopify-recruitment-task"

  def welcome_email(user)
    @user = user
    mail(:to => user.email, :subject => "Welcome to Ultimate Pet!")
  end
end
