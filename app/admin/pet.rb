ActiveAdmin.register Pet do
  permit_params :name, :age, :sex, :type

  index do
    selectable_column
    id_column
    column :name
    column :age
    column :sex
    column :type
    column :created_at
    actions
  end

  filter :name
  filter :age
  filter :sex
  filter :type
  filter :created_at

  form do |f|
    f.inputs "Pet Details" do
      f.input :name
      f.input :age
      f.input :sex
      f.input :type
    end
    f.actions
  end
end
