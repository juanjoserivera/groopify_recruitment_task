ActiveAdmin.register Fight do
  permit_params :time, :winner_id, pet_ids: []

  index do
    selectable_column
    id_column
    column :time
    column :winner_id
    column :created_at
    actions
  end

  show do |fight|
    attributes_table do
      row :id
      row :time
      row :winner_id
      row :created_at
    end
    panel "Pets" do
      table_for fight.pets do
        column :id
        column :name
        column :age
        column :sex
        column :type
      end
    end
  end

  filter :time
  filter :winner_id
  filter :created_at

  form do |f|
    f.inputs "Pet Details" do
      f.input :time
      f.input :winner_id, :label => 'Elige a la mascota ganadora', :as => :select, :collection => Pet.all.map{|p| [p.name.capitalize, p.id]}
      f.input :pets, label: "Pet1", :as => :select, :collection => Pet.all.map{|p| [p.name.capitalize, p.id]}
      f.input :pets, label: "Pet2", :as => :select, :collection => Pet.all.map{|p| [p.name.capitalize, p.id]}
    end
    f.actions
  end
end
