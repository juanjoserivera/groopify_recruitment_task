class Fight < ActiveRecord::Base
  has_and_belongs_to_many :pets

  accepts_nested_attributes_for :pets

  validate :on => [:create, :update] do
    if self.pets.size > 2
      errors.add :fight, "You can only add 2 pets per fight. Please remove one."
    end

    if self.pets.fight_today?
      errors.add :fight, "You can't add this pet, It fights today. Please remove"
    end

    if self.pets.same_owner?
      errors.add :fight, "You can't add this pet, two pets has the same owner. Please remove"
    end
  end

  validate :check_pets_same_day


  # Class methods

  # Instance methods

  def winner
    Pet.where(id: self.winner_id)
  end
end
