class Pet < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :fights

  validates_presence_of :name, :type

  scope :dog, -> { where(id: Dog.all) }
  scope :rat, -> { where(id: Rat.all) }
  scope :chinchilla, -> { where(id: Chinchilla.all) }

  TYPES = { 1 => 'Dog',
            2 => 'Rat',
            3 => 'Chinchilla'
          }

  # class methods

  def self.fight_today?
    self.all.each do |pet|
      pet.fights.each do |p|
        if p.time == Date.today
          return false
        end
      end
    end
    true
  end

  def self.same_owner?
    owners = self.map(&:user_id)
    if owners.uniq.size > 2
      return false
    end
    true
  end


  # Instance methods

  def type
    self.class.to_s
  end
end
