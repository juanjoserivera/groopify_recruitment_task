class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :user_metadatum
  has_many :pets

  before_create :generate_auth_token
  after_create :send_welcome_email

  validates :auth_token, uniqueness: true

  scope :auth_token, -> (auth_token) { where auth_token: auth_token }

  # class methods


  # Instance methods

  def is_owner? pet
    return pet.user == self ? true : false
  end

  private

  def generate_auth_token
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

  def send_welcome_email
    UserMailer.welcome_email(self).deliver_now
  end
end
