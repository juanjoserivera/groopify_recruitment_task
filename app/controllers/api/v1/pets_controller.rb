class Api::V1::PetsController < ApplicationController

  before_filter :set_resources, only: [:show, :update, :destroy]
  before_action :authenticate

  def show
    render json: @pet.as_json( only: [:id, :name, :age, :sex, :victories], methods: [:type], include: { user: { only: [ :id, :email ] } } )
  end

  def create
    begin
      pet = params[:type].capitalize.constantize.new(pet_params)
    rescue
      render json: { "errors" => "You must indicate a valid type of PET!" }
    end

    if pet.save
      render json: { pet.name => "Created successfully!" }
    else
      render json: { "errors" => pet.errors }
    end
  end

  def update
    unless @user.is_owner?(@pet)
      render json: { "errors" => "You must be the owner to change it" }
    else
      if @pet.update_attributes(pet_params)
        render json: { @pet.name => "Updated successfully!" }
      else
        render json: { "errors" => @pet.errors }
      end
    end
  end

  def destroy
    unless @user.is_owner?(@pet)
      render json: { "errors" => "You must be the owner to destroy it" }
    else
      @pet.destroy
      render json: { "result" => "Pet destroy successfully!" }
    end
  end

  private

  def pet_params
    params[:user_id] = @user.id
    params.permit(:name, :age, :sex, :user_id)
  end

  def authenticate
    @user = User.auth_token(params[:auth_token]).first
    return @user.present? ? @user : (render json: { "errors" => "User doesn't exists. Try to change auth_token params" })
  end

  def set_resources
    begin
      @pet = Pet.find params[:id]
    rescue
      render json: { "errors" => "Pet doesn't exists. Try to change id params" }
    end
  end
end
