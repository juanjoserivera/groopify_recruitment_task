class FightsPets < ActiveRecord::Migration
  def change
    create_table :fights_pets, :id => false do |t|
      t.references :fight, :null => false
      t.references :pet, :null => false
    end
    add_index(:fights_pets, [:fight_id, :pet_id], :unique => true)
  end
end
