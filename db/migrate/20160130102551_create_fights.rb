class CreateFights < ActiveRecord::Migration
  def change
    create_table :fights do |t|
      t.datetime :time
      t.integer :winner_id

      t.timestamps null: false
    end
  end
end
